*good*
  ★ awesome
  ★ incredible
  ★ marvellous
——
*important*
  ★ requisite
  ★ essential
  ★ indispensable
——
*angry*
  ★ provoked
  ★ enraged
——
*hardworking*
  ★ industrious
  ★ studious
——
*brave*
  ★ courageous
  ★ adventurous
——
*generous*
  ★ gracious
  ★ magnanimous
——
*humorous*
  ★ witty
  ★ amusing
——
*ambitious*
  ★ aggressive
  ★ competitive
——
*worried*
  ★ uneasy
  ★ apprehensive
——
*beautiful*
  ★ exquisite
  ★ charming
  ★ gorgeous
——
*expensive*
  ★ pricey
  ★ exorbitant
  ★ extortionate
——
*delicious*
  ★ tasty
  ★ appetising
——
*careless*
  ★ remiss
——
*tired*
  ★ exhausted
——
*enough*
  ★ adequate
——
*advanced*
  ★ state-of-the-art
  ★ up-to-date